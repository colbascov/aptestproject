from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    """модель Post"""
    title = models.CharField('заголовок поста', blank=False, max_length=120)
    content = models.TextField('контент', max_length=1500, blank=True)
    created_at = models.DateTimeField('дата создания', auto_now_add=True, auto_now=False)
    author = models.ForeignKey(User, null=True, related_name='posts', on_delete=models.CASCADE)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return '{} - {}'.format(self.title, self.author)
