from django.core.exceptions import FieldError
from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User

from app.forms import PostForm
from app.models import Post


def index(request):
    posts = Post.objects.all().order_by('-created_at')
    return render(request, 'index.html', context={'posts': posts})


def post(request, pk):
    post = Post.objects.get(pk=pk)
    return render(request, 'post.html', context={'post': post})


def profile(request):
    form = PostForm(request.POST or None)

    if request.POST:
        try:
            user = User.objects.get(user=request.user)
        except FieldError:
            user = None
        if form.is_valid():
            new_post = form.save(commit=False)
            new_post.author = user
            new_post.save()
        return redirect('index')

    return render(request, 'profile.html', context={'form': form})


def logout(request):
    auth_logout(request)
    return redirect('/')
